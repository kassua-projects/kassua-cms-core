<?php

namespace Kassua\CMSCore\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route(path: '/', name: 'dashboard')]
    public function dashboard()
    {
        return $this->render('@KassuaCMSCore/dashboard/index.html.twig');
    }
}
