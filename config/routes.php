<?php

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->import('../src/Controller', 'attribute')->prefix('/admin');

//    $routes->add('kassua_cms', '/admin')
//        ->controller([\Kassua\CMSCore\Controller\AdminController::class, 'default']);
};
